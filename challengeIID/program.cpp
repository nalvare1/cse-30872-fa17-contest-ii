#include<iostream>
#include<algorithm>
#include<string>
#include<vector>
#include<set>
#include<stdlib.h>
using namespace std;

bool isInsertOrRemove(string s1, string s2) {//s1 is the longer string; insertion or removal
	int count = 0;
	for(size_t i = 0; i < s1.length(); i++) {
		if(s1[i] != s2[i]) {
			count++;
			if(s1[i+1] != s2[i] || count > 1) {
				return false;
			} else {
				i++;
			}
		}
	}
	return true;
}

bool oneNotZeroAway(string s1, string s2) {
	int s1L = int(s1.length());
	int s2L = int(s2.length());
	//check if length is the same
	if(s1.length() == s2.length()) {
		int count = 0;
		for(size_t i = 0; i < s1.length(); i++) {//one or zero characters replaced
			if(s1[i] != s2[i]) {
				count++;
			}
		}
		if(count > 1 || count < 1) {
			return false;
		} else {
			return true;
		}
	} else if(abs(s1L - s2L) == 1) { //one edit away:
		if(s1L - s2L == 1) {
			bool hi = isInsertOrRemove(s1, s2);
			return hi;
		}
		if(s2L - s1L == 1) {
			bool hi = isInsertOrRemove(s2, s1);
			return hi;
		}
	}
	//else: if not zero or one edits away
	return false;
}

int morphing_words(string w, set<string> & words, vector<string> &c, vector<string> &s, int res) {
	size_t result;
	vector<size_t> ans;
	bool found = false;
	cout << "initial found: " << found << endl;
	if(words.empty()) {
		cout << "base: w = " << w << endl;//"; result = " << res << endl;
	//	for(size_t i = 0; i < c.size(); i++) {
	//		s.push_back(c[i]);
	//	}
		cout << "res = 0" << endl;
		return res;
	} else {
		cout << "set:\n";
		for(auto it=words.begin(); it!=words.end(); it++) {
			cout << *it << " ";
		}
		cout << endl;
		for(auto it=words.begin(); it!=words.end(); it++) {
			string w2=*it;
			if(oneNotZeroAway(w, w2)) {
				words.erase(w2);
			//	c.push_back(w2);

				cout << "recurse: w = " << w << "; w2 = " << w2 << endl;//"; result = " << res << endl;

				result = morphing_words(w2, words, c, s, res+1);
				ans.push_back(result);
				cout << "w1 = " << w << " w2 = " << w2 << "result: " << result << endl;

				words.insert(w2);
			//	c.pop_back();

				found=true;
				cout << "true found " << found << endl;
			}

			cout << "found: " << found << endl;
		}
		if(found == false) {
			cout << "res = 0" << endl;
		//	for(size_t i = 0; i < c.size(); i++) {
		//		s.push_back(c[i]);
		//	}
			cout << "here!";
			return res;
		}

	//find max:
	//size_t max = 0;
	//for(size_t i = 0; i < ans.size(); i++) {
	//	if(ans > max) {
			
	
		sort(ans.begin(), ans.end());
		size_t max = ans[ans.size()-1];
		cout << "res = " << max+1;
		return result;
	}
}


int main(int argc, char * argv[]) {
	string word;
	vector<string> words;
	set<string> words_s;

	while(cin >> word) {
		words.push_back(word);
		words_s.insert(word);
		cout << word << endl;
	}

	int ans,  max=0, result=0;
	vector<string> c, s;

	for(size_t i = 0; i < words.size(); i++) {
		cout << "currentW: " << words[i] << endl;
		words_s.erase(words[i]);
		ans = morphing_words(words[i], words_s,c, s, result);
		words_s.insert(words[i]);
		if(ans > max) {
			max = ans;
		//	for(size_t i = 0; i < max_v.size(); i++) {
		//		if(
		}
	}
	cout << "result: " << ans << endl;

	return 0;
}

