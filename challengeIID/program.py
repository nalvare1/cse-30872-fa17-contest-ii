#!/usr/bin/env python2.7

import sys

def findMorphs(words):
	word_map = {k: [] for k in words}
	for w in words:
		ret = []
		#mList2 = []
		newL = [i for i in words if i != w]
		word_map[w] = isOneAway(w, newL)

	for w in words:
		if len(word_map[w]) == 0:
			word_map.pop(w)
#	for i in word_map:
#		print i, " ", word_map[i]
		
	return word_map

def isInsertOrRemove(word1, word2):
	count = 0
	for i in range(0, len(word1)):
		if i >= len(word2):
			if count == 0:
				return 1
			else:
				return 2
		else:
			if word1[i] != word2[i]:
				count+=1
				if word1[i+1] != word2[i] or count > 1:
					return 2
				else:
					i+=1
	return 1


def isOneAway(word1, otherWords):
	otherWords = sorted(otherWords, key = len)
	mList = []
	for word2 in otherWords:
	#	print "here!", word1, " ", word2
		if len(word1) == len(word2):
			count = 0
			for i in range(0, len(word1)):
				if word1[i] != word2[i]:
					count+=1
			if count != 1:
				continue
			else:
				if(word1 < word2):
					mList.append(word2)
		elif abs(len(word1)-len(word2)) == 1:
			if len(word1) > len(word2):
				hi = isInsertOrRemove(word1, word2)
				if hi == 2:#2 = false here
					continue
				else:
					if(word1 < word2):
						mList.append(word2)
			else:
				hi = isInsertOrRemove(word2, word1)
				if hi == 2:
					continue
				else:
					if(word1 < word2):
						mList.append(word2)
		
	return mList

def DFS_rec(v, prev, word_map, marked, r):
	if v in marked:
		if len(r) > 0:
			r.pop()
		return r
	if v not in word_map:#len(word_map[v]) == 0:
		r.append(v)
		return r
	
#	print v
	#process:
	ans = []
	
	r.append(v)
	marked.add(v) #marked[currWord] = prevWord
	
	for u in word_map[v]:
	#	print "v: ", v, "hi: ", hi
	#	print "u: ", u
		ans1 = []
		ans1 = DFS_rec(u, v, word_map, marked, r)
	#	print "ans1: ", ans1
		ans.append(list(ans1))
		if len(r) > 0:
			r.pop()
		
	maxL = 0
	maxvect = []
	#print "ans:"
	for i in ans:
	#	print i
		if len(i) > maxL:
			maxL = len(i)
			maxvect = i
	#"maxvect:"
	#print "maxvect: ", maxvect
	return maxvect
			
			
# Main execution:	
	
words = []
set1 = set()
for line in sys.stdin:
	word = line.strip()
	words.append(word)
#	print words
#for i in words:
#	print i
word_map = {k: [] for k in range(len(words))}
word_map = findMorphs(words)

hi = []
ans_tot = []
for w in words:
	s = []

	marked = set()
	hi = DFS_rec(w, w, word_map, marked, s)
	
	hi2 = []
	for i in range(0, len(hi)):
		hi2.append(hi[i])
	ans_tot.append(hi2)
	
maxVect = []
maxL = 0
for i in ans_tot:
	if len(i) > maxL:
		maxL = len(i)
		maxVect = i

# print results:
print maxL
for i in maxVect:
	print i
