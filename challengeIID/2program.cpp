#include<iostream>
#include<string>
#include<vector>
#include<set>
#include<stdlib.h>
using namespace std;

bool isInsertOrRemove(string s1, string s2) {//s1 is the longer string; insertion or removal
	int count = 0;
	for(size_t i = 0; i < s1.length(); i++) {
		if(s1[i] != s2[i]) {
			count++;
			if(s1[i+1] != s2[i] || count > 1) {
				return false;
			} else {
				i++;
			}
		}
	}
	return true;
}

bool oneOrZeroAway(string s1, string s2) {
	int s1L = int(s1.length());
	int s2L = int(s2.length());
	//check if length is the same
	if(s1.length() == s2.length()) {
		int count = 0;
		for(size_t i = 0; i < s1.length(); i++) {//one or zero characters replaced
			if(s1[i] != s2[i]) {
				count++;
			}
		}
		if(count > 1) {
			return false;
		} else {
			return true;
		}
	} else if(abs(s1L - s2L) == 1) { //one edit away:
		if(s1L - s2L == 1) {
			bool hi = isInsertOrRemove(s1, s2);
			return hi;
		}
		if(s2L - s1L == 1) {
			bool hi = isInsertOrRemove(s2, s1);
			return hi;
		}
	}
	//else: if not zero or one edits away
	return false;
}

int morphing_words(string w, string w2, vector<string> words, int result, int maxR, set<string> &s, vector<string> & result_v) {
	int max = 0;
	if(!oneOrZeroAway(w, w2) || result == maxR) {
		cout << "base case: w = " <<  w << " and w2 = " << w2 << "; result = " << result << endl;
		return result;
	} else {
		cout << "resursive case: w = " <<  w << " and w2 = " << w2 << endl;
		for(size_t i = 0; i < words.size(); i++) {
			if(s.find(words[i]) == s.end()) {//need to change to set! (aka not in s)
				//result_v.push_back(words[i]);
				s.insert(words[i]);
				int ans = morphing_words(w2, words[i], words, result+1, maxR, s, result_v);
				//result_v.pop_back();
				s.erase(words[i]);
				if(ans > max) {
					max = ans;
					for(size_t i = 0; i < result_v.size(); i++) {
						max_v.push_back(result_v[i]);
					}
					//max_v = result_v;
				}
			}
		}
	}
	return max;
}


int main(int argc, char * argv[]) {
	string word;
	vector<string> words;

	while(cin >> word) {
		words.push_back(word);
		cout << word << endl;
	}

	int result = 0; int ans; int max = 0;
	int maxR = int(words.size());
	vector<string> results, max_v, max_v2;
	set<string> s;

	for(size_t i = 0; i < words.size(); i++) {
		results.push_back(words[i]);
		s.insert(words[i]);
		cout << "currentW: " << words[i] << endl;
		ans = morphing_words(words[i], words[i], words, result, maxR, s, results, max_v);
		results.pop_back();
		s.erase(words[i]);
		if(ans > max) {
			max = ans;
		//	for(size_t i = 0; i < max_v.size(); i++) {
		//		if(
		}
		cout << "result: " << ans << endl;
	}

	return 0;
}

