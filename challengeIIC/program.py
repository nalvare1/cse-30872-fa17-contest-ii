#!/usr/bin/env python2.7

import sys
import itertools

#Contest IIC
#Matching Permutations

def findPerms(wordList):
	#word list contains two words
	minLength = min(len(wordList[0]), len(wordList[1]))
	myPerm = []
	found = 0
	if len(wordList[0]) == minLength:
		maxWord = wordList[1]
		minWord = wordList[0]
	else:
		maxWord = wordList[0]
		minWord = wordList[1]
	#Iterate backwards from the minimum word length
	for i in range(minLength, 0, -1):
		for perm in itertools.permutations(maxWord, i):
			findCount = 0
			l = list(minWord)
			for p in perm:
				if p in l:
					findCount+=1
					l.remove(p)
			if findCount == len(perm):
				perm = sorted(perm)
				myPerm = perm
				found = 1
				break
		if found:
			break
	return myPerm

if __name__ == '__main__':
	wordList = []
	count = 0
	for line in sys.stdin:
		line = line.strip()
		wordList.append(line)
		count += 1
		if count == 2:
			perms = findPerms(wordList)
			count = 0
			wordList = []
			str1 = ''.join(str(e) for e in perms)
			print str1
