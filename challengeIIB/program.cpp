#include <iostream>
#include <vector>
#include<algorithm>

using namespace std;

vector<int> find_path(int endRow, int n, int m, int ** matrix, int ** memo_table){
//populate table with min paths:
	int UpDiag, Left, DownDiag;

	for(int ir =0; ir < m; ir++) {
		for(int ic=0; ic<n; ic++) {
			//perform memoizatino:
			if(ir+1 < m) {
				DownDiag = memo_table[ir+1][ic-1];
			} else {
				DownDiag = memo_table[0][ic-1];
			}
			Left = memo_table[ir][ic-1];
			if(ir-1 >= 0) {
				UpDiag = memo_table[ir-1][ic-1];
			} else {
				UpDiag = memo_table[m-1][ic-1];
			}
			//favor UpDiag, then Left, then DownDiag:
			if(UpDiag <= Left && UpDiag <= DownDiag) {
				memo_table[ir][ic] = UpDiag + matrix[ir][ic];
			} else if(Left <= UpDiag && Left <= DownDiag) {
				memo_table[ir][ic] = Left + matrix[ir][ic];
			} else {
				memo_table[ir][ic] = DownDiag + matrix[ir][ic];
			}
		}
	}
	//print matrix:
/*	cout << "memo table: " << endl;
	for(int i = 0; i < m; i++){
		for(int j = 0; j < n; j++){
			cout << memo_table[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl;
*/

	//use memoization table to find min path:
	vector<int> path, v, rows;
	int ir=endRow; int ic=n-1;
	int ur, dr, lr;
	path.push_back(memo_table[ir][ic]);
	path.push_back(ir);

	while(ic > 0) {//get to any point in the first column
		if(ir+1 < m) {
			DownDiag = memo_table[ir+1][ic-1]; //favor up
			dr = ir+1;
		} else {
			DownDiag = memo_table[0][ic-1];
			dr = 0;
		}
		v.push_back(DownDiag);
		v.push_back(dr);
		//cout << "ir: " << ir << " , ic: " << ic << endl;
		Left = memo_table[ir][ic-1]; //then right
		lr = ir;
		v.push_back(Left);
		v.push_back(lr);
		if(ir-1 >= 0) {
			UpDiag = memo_table[ir-1][ic-1];
			ur = ir-1;
		} else {
			UpDiag = memo_table[m-1][ic-1];
			ur=m-1;
		}
		v.push_back(UpDiag);
		v.push_back(ur);
		int min1 = min(UpDiag, DownDiag);
		int min2 = min(min1, Left);
		for(size_t i = 0; i < v.size(); i+=2) {
			if(v[i] == min2) {
				rows.push_back(v[i+1]);
			}
		}
		sort(rows.begin(), rows.end());
		ir = rows[0];
		
	/*	if(UpDiag <= Left && UpDiag <= DownDiag) {
			if(UpDiag < Left && UpDiag < DownDiag) {
				if(b2) {
					ir--;
				} else {
					ir=m-1;
				}
			} else {
				if(Left == UpDiag && lr < ur && lr < dr) {
					ir=ir+0;
				} else if (DownDiag == UpDiag && dr < ur) {
					if(b1) {
						ir++;
					} else {
						ir=0;
					}
				} else {
					ir=m-1;
				}
			}
		} else if (Left <= UpDiag && Left <= DownDiag) {
			ir=ir+0;
		} else {
			if(b1) {
				ir++;
			} else {
				ir=0;
			}
		}
	*/	path.push_back(ir);
		ic--;
		v.clear();
		rows.clear();
	}
	return path;
}

int main(int argc, char * argv[]){
	// read in graph
	int m, n;
	//int matrix[100][100];
	//int cost[100][100];
	//int path[100][100];
	int temp;
	while(cin >> m >> n) {
		//initialize matrices:
		int ** matrix = new int*[m];
		int ** cost = new int*[m];
		for(int i = 0; i < m; i++) {
			matrix[i] = new int[n];
			cost[i] = new int[n];
		}

		//read in data:
		//cout << m << " " << n << endl;
		for(int ir = 0; ir < m; ir++){
			for(int ic = 0; ic < n; ic++){
				cin >> temp;
				//cout << temp << " ";
				matrix[ir][ic] = temp;
				cost[ir][ic] = 10000;
			}
			//cout << endl;
		}

		//print matrix:
	/*	for(int i = 0; i < m; i++){
			for(int j = 0; j < n; j++){
				cout << matrix[i][j] << " ";
			}
			cout << endl;
		}
		cout << endl;
	*/
		// compute the minimum path
		int min = 10000;
		vector<int> path1;
		vector<int> minPath;
		for(int i = 0; i < m; i++){
			path1.clear();
			path1 = find_path(i, n, m, matrix, cost);
		/*	cout << path1[0] << endl;
			for(size_t j = 1; j < path1.size(); j++) {
				cout << j << " ";
			}
			cout << endl;
		*/	if(path1[0] < min) {
				min = path1[0];
				minPath.clear();
				for(size_t i = 0; i < path1.size(); i++) {
					minPath.push_back(path1[i]);
				}
			}
		}

		//print results:
		cout << minPath[0] << endl;
		int count = 0;
		for(size_t j = minPath.size()-1; j > 0; j--) {
			if(count > 0) {
				cout << " ";
			}
			cout << minPath[j]+1;
			count++;
		}
		cout << endl;

		minPath.clear();
		path1.clear();
		//delete matrices:
		for(int i = 0; i < m; i++) {
			delete [] matrix[i];
			delete [] cost[i];
		}
		delete [] matrix;
		delete [] cost;
	}
	return 0;
}
