#include <iostream>
#include <vector>

using namespace std;

int findMin(int i, int j, int n, int m, int (&matrix)[100][100], int (&cost)[100][100], int (&path)[10000][10000]);

int findMin(int i, int j, int n, int m, int (&matrix)[100][100], int (&cost)[100][100], int (&path)[10000][10000]){
	if(j > n)
		return 0;
	if(cost[i][j] != 99999)
		return cost[i][j]; // already calculated

	// determine the possible paths to take
	int possPaths[3] = {0, 0, 0};
	if(i == 0){
		possPaths[0] = m - 1; // horizontal cylinder
	}
	else{
		possPaths[0] = i - 1;
	}

	possPaths[1] = i;

	if(i == m - 1) // at the last row --> wrap around
		possPaths[2] = 0;
	else
		possPaths[2] = i + 1;

	// compute min path from all possible paths
	for(int x = 0; x < 3; x++){
		int pCost = matrix[i][j] + findMin(possPaths[x], j + 1, n, m, matrix, cost, path);
		if(cost[i][j] > pCost)
			cost[i][j] = pCost;
		else if (cost[i][j] == pCost && path[i][j] > possPaths[x]) // keeps ordering
			cost[i][j] = pCost;
		// store the path
		path[i][j] = possPaths[x];
	}

	return cost[i][j];
}


int main(){
	// read in graph
	int m = 5, n = 6;
	int matrix[100][100];
	int cost[100][100];
	int path[10000][10000];
	int temp;
	while(cin >> m >> n){
		cout << m << " " << n << endl;
		for(int i = 0; i < n; i++){
			for(int j = 0; j < n; j++){
				cin >> matrix[i][j];
				cost[i][j] = 99999;
			}
		}
	}
/*
	for(int i = 0; i < m; i++){
		for(int j = 0; j < n; j++){
			cout << matrix[i][j] << " ";
		}
		cout << endl;
	}
*/


	// compute the minimum path
	int lowest = 99999;
	int curr = 0;
	int c;
	for(int i = 0; i < m; i++){
		for(int j = 0; j < n; j++){
			c = findMin(i, j, n, m, matrix, cost, path);
			if(cost[i][0] < lowest){
				curr = i;
				lowest = cost[i][0];
			}
		}
	}
}
