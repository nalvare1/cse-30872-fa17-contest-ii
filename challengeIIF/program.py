#!/usr/bin/env python2.7

import sys
import itertools

def biohack(cals):
	miles = 0
	alreadyEaten = 0
	for taco in cals:
		miles = (2**alreadyEaten) * taco + miles
		alreadyEaten += 1
	print long(miles)

while True:
	line = sys.stdin.readline().strip()
	if line:
		nTacos = int(line)
		cals = []
		cals = map(int, sys.stdin.readline().strip().split())
		calList = itertools.permutations(cals, nTacos)
		calList = sorted(cals,reverse = True)
		#for l in calList:
		#	print l
		biohack(calList)
	else:
		break
