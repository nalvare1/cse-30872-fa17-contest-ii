#!/usr/bin/env python2.7

import sys
import itertools
import collections

#Contest IIA
#Groups
def read_graph(nodes, edges):
	graph = collections.defaultdict(list)	#Adjacency List
	for n in range(1, nodes):
		graph[n] = []
	for i in range(edges):
		line = sys.stdin.readline().strip().split()
		s, t = map(int, line)
		graph[s].append(t)
		graph[t].append(s)
	return graph

def findGroups(graph):
	v = 1
	visited = [0] * 10000
	allgroups = []
	while(sum(visited) < len(graph)): 	#while not all nodes visited
		for i in range(1, len(graph)):
			if visited[i] == 0:
				v = i
				visited[i] = 1
				break
		frontier = []
		marked = set()
		group = []
		frontier.append(v)
		while frontier:
			v = frontier.pop()
			if v in marked:
				continue
			group.append(v)
			visited[int(v)] = 1
			marked.add(v)
			for u in graph[v]:
				frontier.append(u)
		allgroups.append(sorted(group))
	return allgroups

if __name__ == '__main__':
	nodes = sys.stdin.readline().strip().split()
	edges = sys.stdin.readline().strip().split()
	graphNum = 1
	while nodes:
		if nodes==0:
			break
		graph = read_graph(int(nodes[0]), int(edges[0]))
		nodes = sys.stdin.readline().strip().split()
		edges = sys.stdin.readline().strip().split()
		allGroups = findGroups(graph)
		print "Graph", graphNum, "has", len(allGroups), "groups:"	
		for group in allGroups:
			print ' '.join(str(i) for i in group)
		graphNum += 1




