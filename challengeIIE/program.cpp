#include<iostream>
#include<math.h>
using namespace std;

int check(double n11, double n2, int i, int hops) {
	//cout << i << endl;
	int largestHop = --i - 1;
	//cout << "largest " << largestHop << endl;
	while(i > 0) {
		//cout << n11 << " " << i << endl;
		i--;
		n11 = n11+i;
	}
	//if(n11 + 1 == n2 || n11+2 == n2) {
	for(int j = largestHop; j > 0; j--) {
		if(n11+j == n2) {
			hops+=1;
		}
	}
	return hops;
}

int hip_hop(double n1, double n2) {
	int hops = 0; int i = 1; bool odd = false; int n11 = n1; bool oddn1 = false; bool oddn2 = false;

	//find mid and compute distance:
	int mid = ceil( (n1+n2)/2 );
	//cout << "mid: " << mid << endl;

	//account for hopping just frmo say 1 to 2:
	if(mid == n2) {
		return 1;
	}
	while(n11 < mid) {
		//cout << "n1: " << n11 << "; i: " << i << endl;
		n11 = n11 + i;
		i++;
		hops++;
	}
	if(n11 > mid) {
		odd = true;
	}
	//compute hops:
	if(!odd) {
		hops = hops*2;
	} else {
		hops = hops*2-1;
	}

	//check hops:
	if(int(n1) % 2 != 0) {
		oddn1 = true;
	}
	if(int(n2) % 2 != 0) {
		oddn2 = true;
	}
	if((oddn1 && oddn2 && odd) || (oddn1 && !oddn2 && odd) || (!oddn1 && !oddn2 && odd)) {
		hops = check(n11, n2, i, hops);
	}


	return hops;
}


int main(int argc, char*argv[]) {
	double n1, n2;
	int hops;

	while(cin >> n1 >> n2) {
		if(n1 == n2) {
			hops = 0;
		} else {
			hops = hip_hop(n1, n2);
		}
		cout << n1 << " -> " << n2 << " takes " << hops << " hops" << endl;
	}

	return 0;
}
