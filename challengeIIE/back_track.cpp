#include<iostream>
#include<math.h>
using namespace std;


int hip_hop(int nCurr, int n2, int step_size, int hops) {
	int x, y, z;
	x = 100001;
	nCurr += step_size;
	if(nCurr == n2) {
		if (step_size == 1) {
			return 0;
		} else {
			return 100001;
		}
	} else if(nCurr > n2) {
		return 1000001;
	} else {
	/*	if(nCurr == n2) {
			if(step_size == 1) {
				return 0;
			} else {
				return 100001;
			}
		}
	*/
		cout << "nCurr: " << nCurr << "; step_size: "<< step_size << endl;
		if(step_size > 1) {
			x = hip_hop(nCurr, n2, step_size-1, hops+1);
		}
		y = hip_hop(nCurr, n2, step_size, hops+1);
		z = hip_hop(nCurr, n2, step_size+1, hops+1);

		int min = 10000;
		cout << nCurr << ": x " << x << "y " << y << "Z " << z << endl;
		if(x < min) {
			min = x;
		}
		if(y < min) {
			min = y;
		}
		if(z < min) {
			min = z;
		}
		cout << "min+1: " << min+1 << endl;

		return min+1;
	}
}


int main(int argc, char*argv[]) {
	int n1, n2;
	int hops;

	while(cin >> n1 >> n2) {
		if(n1 == n2) {
			hops = -1;
		} else {
			hops = hip_hop(n1, n2, 1, 0);
		}
		cout << n1 << " -> " << n2 << " takes " << hops+1 << " hops" << endl;
	//cout << endl;
	}

	return 0;
}
